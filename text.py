import zmq
import random
import sys
import time
import re
import numpy as np


# port = "5556"
# if len(sys.argv) > 1:
#     port =  sys.argv[1]
#     int(port)

# context = zmq.Context()
# socket = context.socket(zmq.PUB)
# socket.bind("tcp://*:%s" % port)

# fileName="oratory1.txt"
# topic = 42
# # incrementally building a message
# #message_data=""

class text:
	def __init__(self):
		self.sentence=""
		self.wordlist=[]
		self.C=[]
		self.pronoun_list=["I","you","they","he","she","it","me","him","her","my","his","their","ours","yours"]
		self.frequency=[0]*(len(self.pronoun_list))
		self.frequency_1=self.frequency
		self.f_1=0
		self.p_1=""

	# def word_analysis(self):
	# 	self.wordlist=re.split(r'[^0-9A-Za-z]+',self.sentence)

	def sentence_length(self):
		self.wordlist=re.split(r'[^0-9A-Za-z]+',self.sentence)
		return len(self.wordlist)

	def word_length(self):
		self.C=[]
		self.wordlist=re.split(r'[^0-9A-Za-z]+',self.sentence)
		for word in self.wordlist:
			self.C.append(len(word))
		return self.C

	def pron_freq(self):
		self.wordlist=re.split(r'[^0-9A-Za-z]+',self.sentence)
		for item in self.wordlist:
			for i in range(len(self.pronoun_list)):
				if item==self.pronoun_list[i]:
					self.frequency[i]+=1
		self.f_1=max(self.frequency)
		self.p_1=self.pronoun_list[self.frequency.index(max(self.frequency))]
		self.frequency_1=self.frequency[:]
		self.frequency_1[self.frequency.index(max(self.frequency))]=0
		self.f_2=max(self.frequency_1)
		self.p_2=self.pronoun_list[self.frequency_1.index(max(self.frequency_1))]
		return self.f_1, self.p_1,self.f_2, self.p_2



  
'''
def word_count(fileName):
	sentence_end_finder=re.compile(r"[\.\?!]")
	with open(fileName) as myFile:
		C=[]
		count=0
		for line in myFile:
			for word in line.split():
				count+=1
				if sentence_end_finder.findall(word):
					#print "%d %s" % (topic, message_data)
					#socket.send("%d %s" % (topic, message_data))
					C.append(count)
					count=0
					#time.sleep(0.1)
	return C, np.mean(C), min(C), max(C)

def word_length(filename):
	with open(fileName) as myFile:
		L=[]
		for line in myFile:
			for word in line.split():
				L.append(len(word))
	return L, np.mean(L), min(L), max(L)




def pron_freq(fileName,pronoun_list):
	frequency=[0]*(len(pronoun_list))
	with open(fileName) as myFile:
		for line in myFile:
				for word in line.split():
					for i in range(len(pronoun_list)):
						if word==pronoun_list[i]:
							frequency[i]+=1
	f_1=max(frequency)
	p_1=pronoun_list[frequency.index(max(frequency))]
	return frequency, f_1, p_1




C, C_ave, C_min, C_max=word_count(fileName)
L,L_ave, L_min, L_max=word_length(fileName)
frequency, f_1, p_1=pron_freq(fileName,pronoun_list)
#print L
print C_ave, C_min, C_max
print L_ave, L_min, L_max
print frequency, f_1, p_1
'''
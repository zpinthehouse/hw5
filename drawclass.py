import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import zmq
import re
class Meter:
    # Constructor
    def __init__(self, ):
        self.result_green = ""
        self.result_purple = ""
        self.X = [0, 0]
        self.Y = [0, 0]
        self.fig , self.ax = plt.subplots()
        # self.sc = ax.scatter(self.X,self.Y)
        # self.ax.set_xlim(0,15)
        # self.ax.set_ylim(0,2000)
        self.port_send1 = "5555"
        self.port_send2 = "5557"
        self.context = zmq.Context()
        self.results_receiver_green = self.context.socket(zmq.SUB)
        self.results_receiver_purple = self.context.socket(zmq.SUB)
        print "Listening to the result:"
        self.results_receiver_green.connect ("tcp://localhost:%s" % self.port_send1)
        self.results_receiver_green.setsockopt(zmq.SUBSCRIBE, "53")
        self.results_receiver_purple.connect ("tcp://localhost:%s" % self.port_send2)
        self.results_receiver_purple.setsockopt(zmq.SUBSCRIBE, "55")
    def setvalue(self,green,purple):
        self.green = green
        self.purple = purple

    # Print the polygon
    def update(self,_):
        self.result_green = self.results_receiver_green.recv()
        self.result_purple = self.results_receiver_purple.recv()
        self.X[0] +=1 
        self.X[1] += 1
        self.Y[0] = float(self.result_green.split(' ')[1])
        self.Y[1] = float(self.result_purple.split(' ')[1])
        point1 = self.ax.scatter(self.X[0],self.Y[0],color="green")
        point1.set_offsets(np.column_stack((self.X[0],self.Y[0])))
        point2 = self.ax.scatter(self.X[1],self.Y[1],color="purple")
        point2.set_offsets(np.column_stack((self.X[1],self.Y[1])))
        
        return point1, point2,

   

